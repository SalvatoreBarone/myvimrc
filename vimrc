set nocompatible              " be iMproved, required
filetype off                  " required
set exrc					  " Enable local rc

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Grammarous
" Plugin 'rhysd/vim-grammarous'

" fzf
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'

" Lightline
Plugin 'itchyny/lightline.vim'

" Local vimrc
Plugin 'LucHermitte/lh-vim-lib'
Plugin 'LucHermitte/local_vimrc'

" Nerdtree
Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'

" Syntastic Linter
Plugin 'scrooloose/syntastic'

" Purify color scheme
" Plugin 'kyoz/purify', {'rtp': 'vim/'}

" Tender color scheme
Plugin 'jacoborus/tender.vim'

" Clang complete
" Plugin 'justmao945/vim-clang'

" Gutentag
Plugin 'ludovicchabant/vim-gutentags'

" vi-gitgutter
Plugin 'airblade/vim-gitgutter'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" All of your Plugins must be added before the following line
call vundle#end()            " required

filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line



set tags=tags;/
set updatetime=100
set nowrap
syntax on
set number
set noshowmode
set smartindent noexpandtab tabstop=4 shiftwidth=4


set colorcolumn=81
highlight ColorColumn ctermbg=darkgray
colorscheme tender

" mapping for NERDtree
map <C-n> :NERDTreeToggle<CR>


" Syntastic configuration
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes':   [],'passive_filetypes': [] }
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_c_check_header = 1
let g:syntastic_c_compiler_options = '-Wall -Wextra -std=gnu99'
let g:syntastic_cpp_check_header = 1
let g:syntastic_cpp_compiler_options = '-Wall -Wextra -std=gnu99'

"let g:clang_exec = 'clang-4.0'


" DoxygenToolkit
let g:DoxygenToolkit_briefTag_pre="@brief  "
let g:DoxygenToolkit_paramTag_pre="@param "
let g:DoxygenToolkit_returnTag="@returns   "
"let g:DoxygenToolkit_blockHeader="--------------------------------------------------------------------------"
"let g:DoxygenToolkit_blockHeader=""
"let g:DoxygenToolkit_blockFooter="----------------------------------------------------------------------------"
"let g:DoxygenToolkit_blockFooter=""
let g:DoxygenToolkit_authorName="Salvatore Barone <salvator.barone@gmail.com>"
let g:DoxygenToolkit_licenseTag="Copyright (C) 2020 Salvatore Barone <salvator.barone@gmail.com>\<enter>\<enter>"
let g:DoxygenToolkit_licenseTag=g:DoxygenToolkit_licenseTag . "This program is free software; you can redistribute it and/or modify\<enter>"
let g:DoxygenToolkit_licenseTag=g:DoxygenToolkit_licenseTag . "it under the terms of the GNU General Public License as published by\<enter>"
let g:DoxygenToolkit_licenseTag=g:DoxygenToolkit_licenseTag . "the Free Software Foundation; either version 3 of the License, or\<enter>"
let g:DoxygenToolkit_licenseTag=g:DoxygenToolkit_licenseTag . "any later version.\<enter>\<enter>"
let g:DoxygenToolkit_licenseTag=g:DoxygenToolkit_licenseTag . "This program is distributed in the hope that it will be useful,\<enter>"
let g:DoxygenToolkit_licenseTag=g:DoxygenToolkit_licenseTag . "but WITHOUT ANY WARRANTY; without even the implied warranty of\<enter>"
let g:DoxygenToolkit_licenseTag=g:DoxygenToolkit_licenseTag . "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\<enter>"
let g:DoxygenToolkit_licenseTag=g:DoxygenToolkit_licenseTag . "GNU General Public License for more details.\<enter>\<enter>"
let g:DoxygenToolkit_licenseTag=g:DoxygenToolkit_licenseTag . "You should have received a copy of the GNU General Public License\<enter>"
let g:DoxygenToolkit_licenseTag=g:DoxygenToolkit_licenseTag . "along with this program.  If not, see <http://www.gnu.org/licenses/>.\<enter>"


set secure " Prevent command exec from local rc
